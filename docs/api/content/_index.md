# VENTILADOR COM SENSOR DHT11 COM ARDUINO

## Introdução ao tema
O projeto é sobre um ventilador que  será acionado quando a temperatura 
alcançar aos 27°C por meio de um sensor DHT11, aonde o mesmo será desligado 
quando a temperatura chegar abaixo ou igual a 26°C  assim acionando uma 
luz de LED,  e podemos cuidar a oxilação das temperaturas por meio do
monitor serial .
Obs: A temperatura é definida pelo programador.

## Objetivos
Este projeto tem objetivo de mostrar como programar e montar um ventilador 
com sensor de temperatura para que aja facilidade, comodidade e custo baixo
para qualquer pessoa.

## Materiais utilizados

Placa arduino Uno,
Sensor de temperatura DHT11,
Rele com tomada,
Led,
Resistor de 10K,
Fonte de alimentação de 5 volts,
Ventilador.


## Resultados
O projeto foi complicado no início, pois não tinhamos muita experiência em 
arduino, mas com as aulas que o professor Jeferson nos proporcionou fez com que 
nosso conhecimento fosse ficando mais amplo em ralação ao projeto,
também obtivemos ajuda do nosso colega de curso Matheus que foi muito 
eficiente na explicação da programação do projeto.
O resultado fatisfatório.


O vídeo de demonstração pode ser visto em:
{{<youtubeWYAaFDZEbus>}}


### Esquema Elétrico


O esquema elétrico pode ser visto por:

![](foto01.jpeg)



Disponibilizar código do projeto (pasta src/)
Desafios encontrados