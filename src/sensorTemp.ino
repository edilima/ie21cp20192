#include<Adafruit_Sensor.h> 
#include<DHT.h>
#include<DHT_U.h>

#define DHTPIN 2

#define DHTTYPE    DHT11     // DHT 11

DHT_Unified dht(DHTPIN, DHTTYPE);





const int ventilador = 3;
const int led = 4;
float temp = 0;
float humid = 0;

void setup(){
  Serial.begin(9600);
  pinMode(ventilador,OUTPUT);
  pinMode(led,OUTPUT);
  dht.begin();
}

void loop(){

  
  delay(1000);

  sensors_event_t event;
  dht.temperature().getEvent(&event);

  if(isnan(event.temperature)){
    Serial.println(F("Erro no sensor de temperatura"));  
  }
  else{
    temp = event.temperature;
    Serial.print(F("temperature: "));
    Serial.print(temp);
    Serial.println(F("ºC"));

    //Ativar o ventilador se a temperatura desejada for atingida
    if(temp < 27.0){
      digitalWrite(led,HIGH);
      digitalWrite(ventilador,HIGH);
      Serial.println("Desliga ventilador");
    }
    if(temp >= 27.0 && temp < 28.0){
      digitalWrite(led,LOW);
      digitalWrite(ventilador,LOW);
      Serial.println("Liga ventilador");
    }
  }

 }
 