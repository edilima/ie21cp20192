# Introdução
***VENTILADOR COM SENSOR DHT11 COM ARDUINO***

O ventilador será acionado quando a temperatura alcançar os 23°C por meio de um
sensor DHT11, aonde o mesmo será desligado quando a temperatura chegar abaixo de
23°C e assim acionando uma luz de LED.

***O QUE VOCÊ VAI PRECISAR***

Para criar e executar este projeto, você precisará dos seguintes itens:

* Placa arduino Uno
* Sensor de temperatura DHT11
* Rele com tomada
* Led 
* Resisitor de 1K
* Resistor de 10K
* Fonte de alimentação de 5 volts
* Ventilador


***Instalação e configuração***



## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|edilaine de lima -|edilima
|tais remussi|

# Documentação
#include<dht.h>

[fotos do projeto]()
> inserir o link da página de documentação.

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)